﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace OAuth2.Core.Helpers
{
    public class ResponseHelper
    {
        public static Response Redirect(string url)
        {
            var resp = new Response()
            {
                StatusCode = 302,
                StatusDescription = "Moved Temporarily"
            };

            resp.Headers["Location"] = url;

            return resp;
        }
    }
}
