﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;
using System.Web.Caching;

namespace OAuth2.Core.Helpers
{
    public class ObSession : Hashtable
    {
        public string SessionId { get; protected set; }

        public ObSession()
        {
        }

        public static ObSession New()
        {
            var ms = new ObSession();
            ms.SessionId = Guid.NewGuid().ToString("N");
            return ms;
        }
    }

    public class SessionManager
    {
        protected const string COOKIE_NAME = "OAuth2.Session";

        public static ObSession GetFromRequest(Request request)
        {
            ObSession s = null;

            // check for our cookie
            List<Cookie> cookies = Handle.IncomingRequest.Cookies.Where(val => !string.IsNullOrEmpty(val)).Select(x => new Cookie(x)).ToList();
            Cookie cookie = cookies.FirstOrDefault(x => x.Name == COOKIE_NAME);

            if(cookie!=null)
            {
                // load from cache
                var cache = new Cache();
                s = cache[cookie.Value] as ObSession;
            }

            if (s == null)
                s = NewSession();

            return s;
        }

        public static ObSession NewSession()
        {
            return ObSession.New();
        }

        public static Response SetOnResponse(Response response, ObSession session)
        {
            // store session in Cache
            var cache = new Cache();
            cache.Insert(session.SessionId, session, null, DateTime.UtcNow.AddMinutes(20), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);

            // append our "session"-cookie to outgoing resposne
            Cookie cookie = new Cookie(COOKIE_NAME, session.SessionId);
            response.Cookies.Add(cookie.ToString());

            return response;
        }
    }
}
