﻿using System;
using System.Collections.Generic;
using System.Linq;
using Starcounter;
using OAuth2.Core.Services;
using System.Web;

namespace OAuth2.Core.Helpers
{
    public static class Extensions
    {
        public static void AddRange<T, S>(this IDictionary<T, S> target, IDictionary<T, S> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            foreach (var item in source)
            {
                target[item.Key] = item.Value;
            }
        }

        public static string Get(this Dictionary<string, string> d, string key)
        {
            if (d.ContainsKey(key))
                return d[key];

            return null;
        }

        public static string Combine(this Uri root, string relative)
        {
            string baseUrl = VirtualPathUtility.AppendTrailingSlash(root.ToString());
            return VirtualPathUtility.Combine(baseUrl,relative);
            //return new Uri(root, relative, ).ToString();
        }

        public static string MakeAbsoluteUrl(this Request request, string url, params string[] p)
        {
            // TODO: Need a way to safely identify the url back to our self INCLUDING THE SCHEME(!)
            // as for now, we assume we're running plain http://
            return String.Format("http://{0}{1}", request.Host, String.Format(url, p));
        }
    }
}
