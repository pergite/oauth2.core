﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAuth2.Core.Helpers
{
    public class RequestParameterValue
    {
        public string StringValue;
        public ICollection<string> StringCollection;

        public static implicit operator RequestParameterValue(string s)
        {
            return new RequestParameterValue() { StringValue = s };
        }

        public static implicit operator RequestParameterValue(string[] sa)
        {
            return new RequestParameterValue() { StringCollection = sa };
        }

        public static implicit operator RequestParameterValue(List<string> sa)
        {
            return new RequestParameterValue() { StringCollection = sa };
        }

        public object Value
        {
            get
            {
                if (null != StringValue)
                    return StringValue;

                return StringCollection;
            }
        }
    }

    public enum ArrayHandlingEnum { Duplicate=1, Join=2 };

    public class RequestParameters
    {
        public static string Build(IDictionary<string, string> request_params)
        {
            List<string> params_builder = new List<string>();

            foreach (var item in request_params)
            {
                if (null != item.Value)
                    params_builder.Add(String.Format("{0}={1}", item.Key, System.Uri.EscapeDataString(item.Value)));
            }

            return String.Join("&", params_builder);
        }

        public static string Build(IList<Tuple<string,string>> request_params)
        {
            List<string> params_builder = new List<string>();

            foreach (var item in request_params)
            {
                if (null != item.Item2)
                    params_builder.Add(String.Format("{0}={1}", item.Item1, System.Uri.EscapeDataString(item.Item2)));
            }

            return String.Join("&", params_builder);
        }

        public static string Build(IDictionary<string, RequestParameterValue> request_params, ArrayHandlingEnum ah = ArrayHandlingEnum.Duplicate)
        {
            List<string> params_builder = new List<string>();

            foreach (var item in request_params)
            {
                if (null == item.Value)
                    continue;

                if (null != item.Value.StringValue)
                {
                    params_builder.Add(String.Format("{0}={1}", item.Key, System.Uri.EscapeDataString(item.Value.StringValue)));
                }
                else if (null!=item.Value.StringCollection)
                {
                    if(ah == ArrayHandlingEnum.Duplicate)
                    {
                        foreach (string s in item.Value.StringCollection)
                        {
                            params_builder.Add(String.Format("{0}={1}", item.Key, System.Uri.EscapeDataString(s)));
                        }
                    }
                    else
                    {
                        params_builder.Add(String.Format("{0}={1}", item.Key, System.Uri.EscapeDataString(String.Join(",", item.Value.StringCollection))));
                    }
                }
            }

            return String.Join("&", params_builder);
        }
    }
}
