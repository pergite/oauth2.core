﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

using OAuth2.Core.Helpers;
using OAuth2.Core.Models;

namespace OAuth2.Core.Services
{
    public abstract partial class ServiceBase
    {
        protected const string SESSION_NAME = "OAuth.Signin.CodeVerifier";

        public abstract string SERVICE_NAME { get; }

        public string CLIENT_ID { get; set; }
        public string CLIENT_SECRET { get; set; }

        public abstract string AUTHORIZATION_ENDPOINT { get; }
        public abstract string TOKEN_ENDPOINT { get; }
        public abstract string USERINFO_ENDPOINT { get; }
        public virtual string REFRESH_TOKEN_ENDPOINT {  get { return this.TOKEN_ENDPOINT; } }

        //public virtual IToken Token { get; set; }
        public virtual ITokenManager TokenManager { get; set; }

        protected virtual IDictionary<string, string> GetAuthRequestParams(ICollection<string> scopes)
        {
            return new Dictionary<string, string>();
        }

        protected virtual IDictionary<string, string> GetTokenRequestParams(ICollection<string> scopes)
        {
            return new Dictionary<string, string>();
        }

        protected virtual IDictionary<string, string> GetTokenRefreshParams()
        {
            return new Dictionary<string, string>();
        }

        protected virtual void ModifyAuthRequestParams(IDictionary session, IDictionary<string, string> request_params, ICollection<string> scopes)
        {
            request_params.AddRange(this.GetAuthRequestParams(scopes));
        }

        protected virtual void ModifyTokenRequestParams(IDictionary session, IDictionary<string, string> request_params, ICollection<string> scopes)
        {
            request_params.AddRange(this.GetTokenRequestParams(scopes));
        }

        protected virtual void ModifyTokenRefreshParams(IDictionary<string, string> request_params)
        {
            request_params.AddRange(this.GetTokenRefreshParams());
        }

        protected abstract TokenInfo ParseTokenResponse(string response);
        protected abstract UserInfo ParseUserInfoResponse(string response);
        protected virtual TokenInfo ParseRefreshToken(string response)
        {
            throw new NotImplementedException();
        }

        public virtual string GetAuthEndpoint(IDictionary session, string callbackEndpoint, string state, ICollection<string> scopes, IDictionary<string,string> additionalRequestParameters = null)
        {
            string code_verifier = randomDataBase64url(32);
            session[SESSION_NAME] = code_verifier;
            
            string code_challenge = base64urlencodeNoPadding(sha256(code_verifier));

            Dictionary<string, string> request_params = new Dictionary<string, string>()
            {
                { "response_type", "code" },
                { "redirect_uri", callbackEndpoint },
                { "client_id", this.CLIENT_ID },
                { "state", state },
                { "code_challenge", code_challenge },
                { "code_challenge_method", "S256" }
            };

            ModifyAuthRequestParams(session, request_params, scopes);

            if(null != additionalRequestParameters)
            {
                request_params.AddRange(additionalRequestParameters);

            }

            return String.Format("{0}?{1}", this.AUTHORIZATION_ENDPOINT, RequestParameters.Build(request_params));
        }

        public AuthResponseResult ProcessAuthResponse(IDictionary session, string queryString, string callbackEndpoint, string state, ICollection<string> scopes)
        {
            NameValueCollection query = HttpUtility.ParseQueryString(queryString);

            if (null != query["error"] || null==query["code"])
            {
                return AuthResponseResult.Error(query["error"]);
            }

            string code_verifier = session[SESSION_NAME] as string;

            try
            {
                var token = PerformCodeExchange(session, query["code"], code_verifier, callbackEndpoint, scopes);

                var user = GetUserInfo(token);

                return new AuthResponseResult(true)
                {
                    Token = token,
                    User = user
                };
            }
            catch(ReRunAuthenticationException rex)
            {
                return AuthResponseResult.Error("Rerun authentication", new ReRunAuthentication(rex.AdditionalRequestParameters));
            }
        }

        protected virtual UserInfo GetUserInfo(TokenInfo token)
        {
            // sends the request
            HttpWebRequest userinfoRequest = (HttpWebRequest)WebRequest.Create(this.USERINFO_ENDPOINT);
            userinfoRequest.Method = "GET";
            userinfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", token.AccessToken));
            //userinfoRequest.Accept = "Accept=application/json";

            // gets the response
            WebResponse userinfoResponse = userinfoRequest.GetResponse();
            using (StreamReader userinfoResponseReader = new StreamReader(userinfoResponse.GetResponseStream()))
            {
                // reads response body
                string response = userinfoResponseReader.ReadToEnd();

                return ParseUserInfoResponse(response);
            }
        }

        protected virtual TokenInfo PerformCodeExchange(IDictionary session, string code, string code_verifier, string redirectURI, ICollection<string> scopes)
        {
            Dictionary<string, string> request_params = new Dictionary<string, string>()
            {
                { "code", code },
                { "redirect_uri", redirectURI },
                { "client_id", this.CLIENT_ID },
                { "client_secret", this.CLIENT_SECRET },
                { "grant_type", "authorization_code" },
                { "code_verifier", code_verifier },
                { "scope", "" }
            };

            ModifyTokenRequestParams(session, request_params, scopes);

            // builds the  request
            string tokenRequestBody = RequestParameters.Build(request_params);

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(this.TOKEN_ENDPOINT);

            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=application/json";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            stream.Write(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = tokenRequest.GetResponse();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = reader.ReadToEnd();

                   // converts to dictionary
                   return this.ParseTokenResponse(responseText);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = reader.ReadToEnd();

                            Console.Write(responseText);
                        }
                    }

                }
            }

            return null;
        }

        public virtual IToken RefreshAccessToken(IToken token = null)
        {
            if (null == token)
                token = TokenManager.GetToken();

            if (null == token || null == token.RefreshToken)
                throw new ArgumentNullException("Must be given a valid Token.RefreshToken");

            Dictionary<string, string> request_params = new Dictionary<string, string>()
            {
                { "refresh_token", token.RefreshToken },
                { "client_id", this.CLIENT_ID },
                { "client_secret", this.CLIENT_SECRET },
                { "grant_type", "refresh_token" }
            };

            this.ModifyTokenRefreshParams(request_params);

            // builds the  request
            string tokenRequestBody = RequestParameters.Build(request_params);

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(this.REFRESH_TOKEN_ENDPOINT);

            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=application/json";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;

            Stream stream = tokenRequest.GetRequestStream();
            stream.Write(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = tokenRequest.GetResponse();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = reader.ReadToEnd();

                    // converts to dictionary
                    TokenInfo newToken = this.ParseRefreshToken(responseText);

                    TokenManager.SetToken(newToken);
                    return TokenManager.GetToken();
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = reader.ReadToEnd();

                            Console.Write(responseText);
                        }
                    }

                }
            }

            return null;
        }

        protected static string randomDataBase64url(uint length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return base64urlencodeNoPadding(bytes);
        }

        protected static byte[] sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        protected static string base64urlencodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }
    }

}
