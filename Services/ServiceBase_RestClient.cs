﻿using System;
using System.IO;
using System.Net;
using System.Text;

using Newtonsoft.Json;
using OAuth2.Core.Models;

namespace OAuth2.Core.Services
{
    public class NoContent
    {
        // placeholder class for typed Rest calls that doesn't return any data
        public static NoContent Data
        {
            get
            {
                return new NoContent();
            }
        }
    }

    public class RestException : Exception
    {
        public string Url;
        public int StatusCode;
        public string ResponseContent;

        public RestException(string url, int statusCode, string content) : base()
        {
            Url = url;
            StatusCode = statusCode;
            ResponseContent = content;
        }
    }

    public partial class ServiceBase
    {
        protected int pass = 0;

        protected HttpWebRequest AuthorizeRequest(HttpWebRequest request)
        {
            var token = TokenManager.GetToken();
            if (null == token || null == token.AccessToken)
                throw new InvalidOperationException("Token/AccessToken must not be null");

            if (DateTime.UtcNow >= token.Expires && null!=token.RefreshToken)
                token = this.RefreshAccessToken(token);

            request.Headers["Authorization"] = String.Format("Bearer {0}", token.AccessToken);

            return request;
        }

        protected HttpWebResponse Execute(Func<HttpWebRequest> creator, Stream data=null, Action<Stream> postAction=null)
        {
            int retries = 0;

            again:
            var request = creator();
            this.AuthorizeRequest(request);

            try
            {
                if (null != data || null!=postAction)
                {
                    using (var ws = request.GetRequestStream())
                    {
                        if (null != data)
                            data.CopyTo(ws);
                        else
                            postAction(ws);
                    }
                }

                return (HttpWebResponse)request.GetResponse();
            }
            catch (WebException wex)
            {
                if (null != wex.Response)
                {
                    HttpStatusCode statusCode = ((HttpWebResponse)wex.Response).StatusCode;

                    if (statusCode == HttpStatusCode.Unauthorized)
                    {
                        if (++retries < 2)
                        {
                            RefreshAccessToken();

                            if (null != data)
                                data.Seek(0, SeekOrigin.Begin);

                            goto again;
                        }
                    }
                    string responseContent;
                    using (var sr = new StreamReader(wex.Response.GetResponseStream()))
                    {
                        responseContent = sr.ReadToEnd();
                        Console.WriteLine(responseContent);
                    }

                    throw new RestException(request.Address.ToString(), (int)statusCode, responseContent);
                }
                else throw;
            }
        }

        protected virtual T Deserialize<T>(HttpWebResponse response, Stream dataStream) where T : class
        {
            if (response.StatusCode == HttpStatusCode.NoContent)
                return default(T);

            using (StreamReader sr = new StreamReader(dataStream))
            {
                if (typeof(T) == typeof(string))
                {
                    return sr.ReadToEnd() as T;
                }
                else
                {
                    if (response.ContentType.StartsWith("application/json") || response.ContentType.StartsWith("text/plain"))
                    {
                        return JsonConvert.DeserializeObject<T>(sr.ReadToEnd());
                    }

                    throw new NotSupportedException(String.Format("Dont know what to do with {0}", response.ContentType));
                }
            }
        }

        public virtual Stream Get(string url, string accept="*/")
        {
            HttpWebResponse dontcare;
            return Send(url, "GET", null, null, null, accept, out dontcare);
        }

        public virtual Stream Post(string url, Stream postStream, string contentType, string accept, out HttpWebResponse o_response)
        {
            return Send(url, "POST", postStream, null, contentType, accept, out o_response);
        }

        public virtual Stream Post(string url, Action<Stream> postAction, string contentType, string accept, out HttpWebResponse o_response)
        {
            return Send(url, "POST", null, postAction, contentType, accept, out o_response);
        }

        public virtual Stream Put(string url, Stream postStream, string contentType, string accept, out HttpWebResponse o_response)
        {
            return Send(url, "PUT", postStream, null, contentType, accept, out o_response);
        }

        public virtual Stream Put(string url, Action<Stream> postAction, string contentType, string accept, out HttpWebResponse o_response)
        {
            return Send(url, "PUT", null, postAction, contentType, accept, out o_response);
        }

        public virtual HttpStatusCode Delete(string url)
        {
            HttpWebResponse o_response;
            using (var s = Send(url, "DELETE", null, null, null, "*/*", out o_response))
            {
                // dont expect anything returned, so just leave it
            }

            return o_response.StatusCode;
        }

        public virtual T Get<T>(string url, string accept = "*/*") where T : class
        {
            HttpWebResponse response;
            using (Stream data = this.Send(url, "GET", null, null, null, accept, out response))
            {
                return Deserialize<T>(response, data);
            }
        }

        public virtual T Post<T>(string url, T data, string contentType = "application/json", string accept = "*/*") where T : class
        {
            return this.Send<T, T>(url, "POST", data, contentType, accept);
        }

        public virtual TResult Post<T, TResult>(string url, T data, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            return this.Send<T, TResult>(url, "POST", data, contentType, accept);
        }

        public virtual TResult Post<TResult>(string url, Stream postData, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            return this.Send<TResult>(url, "POST", postData, null, contentType, accept);
        }

        public virtual TResult Post<TResult>(string url, Action<Stream> postAction, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            return this.Send<TResult>(url, "POST", null, postAction, contentType, accept);
        }

        public virtual T Put<T>(string url, T data, string contentType = "application/json", string accept = "*/*") where T : class
        {
            return this.Send<T, T>(url, "PUT", data, contentType, accept);
        }

        public virtual TResult Put<T, TResult>(string url, T data, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            return this.Send<T, TResult>(url, "POST", data, contentType, accept);
        }

        public virtual TResult Put<TResult>(string url, Stream postData, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            return this.Send<TResult>(url, "PUT", postData, null, contentType, accept);
        }

        public virtual TResult Put<TResult>(string url, Action<Stream> postAction, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            return this.Send<TResult>(url, "PUT", null, postAction, contentType, accept);
        }

        public virtual TResult Send<T, TResult>(string url, string method, T data, string contentType="application/json", string accept="*/*") where TResult : class
        {
            //if(contentType != "application/json")
            //    throw new NotSupportedException(String.Format("Dont know what to do with {0}", contentType));

            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data, settings))))
            {
                HttpWebResponse response;
                using (var responseStream = this.Send(url, method, ms, null, contentType, accept, out response))
                {
                    return Deserialize<TResult>(response, responseStream);
                }
            }
        }

        public virtual TResult Send<TResult>(string url, string method, Stream postData, Action<Stream> postAction, string contentType = "application/json", string accept = "*/*") where TResult : class
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            HttpWebResponse response;
            using (var responseStream = this.Send(url, method, postData, postAction, contentType, accept, out response))
            {
                return Deserialize<TResult>(response, responseStream);
            }
        }

        public virtual Stream Send(string url, string method, Stream postStream, Action<Stream> postAction, string contentType, string accept, out HttpWebResponse o_response)
        {
            Func<HttpWebRequest> createWebRequest = () => {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = method;
                request.Accept = accept;

                if(null!=contentType)
                    request.ContentType = contentType;

                return request;
            };

            o_response = this.Execute(createWebRequest, postStream, postAction);
            return o_response.GetResponseStream();
        }
    }
}
