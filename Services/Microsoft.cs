﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

using OAuth2.Core.Models;

namespace OAuth2.Core.Services
{
    public class Microsoft : ServiceBase
    {
        public override string SERVICE_NAME { get { return "Microsoft"; } }

        public override string AUTHORIZATION_ENDPOINT { get { return "https://login.microsoftonline.com/common/oauth2/authorize"; } }
        public override string TOKEN_ENDPOINT { get { return "https://login.microsoftonline.com/common/oauth2/token"; } }
        public override string USERINFO_ENDPOINT { get { return "https://graph.microsoft.com/v1.0/me"; } }

        protected override IDictionary<string, string> GetAuthRequestParams(ICollection<string> scopes)
        {
            return new Dictionary<string, string>()
            {
                { "prompt", "login" }
            };
        }

        protected override IDictionary<string, string> GetTokenRequestParams(ICollection<string> scopes)
        {
            return new Dictionary<string, string>()
            {
                {"client_id", CLIENT_ID },
                {"resource", "https://graph.microsoft.com/" }
            };
        }

        protected override TokenInfo ParseTokenResponse(string response)
        {
            Dictionary<string, string> tk = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            return new TokenInfo()
            {
                AccessToken = tk["access_token"],
                RefreshToken = tk["refresh_token"],
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(TimeSpan.FromSeconds(int.Parse(tk["expires_in"])))
            };
        }

        protected override TokenInfo ParseRefreshToken(string response)
        {
            Dictionary<string, string> tk = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            return new TokenInfo()
            {
                AccessToken = tk["access_token"],
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(TimeSpan.FromSeconds(int.Parse(tk["expires_in"])))
            };
        }

        protected override UserInfo ParseUserInfoResponse(string response)
        {
            MicroMe me = JsonConvert.DeserializeObject<MicroMe>(response);

            return new UserInfo()
            {
                Provider = "Microsoft",
                ProviderId = me.id,
                Name = me.displayName,
                FirstName = me.givenName,
                LastName = me.surname,
                Email = me.mail,
                Locale = me.preferredLanguage
            };
        }
    }

    internal class MicroMe
    {
        public string[] businessPhones { get; set; }
        public string displayName { get; set; }
        public string givenName { get; set; }
        public string id { get; set; }
        public object jobTitle { get; set; }
        public string mail { get; set; }
        public string mobilePhone { get; set; }
        public object officeLocation { get; set; }
        public string preferredLanguage { get; set; }
        public string surname { get; set; }
        public string userPrincipalName { get; set; }
    }
}