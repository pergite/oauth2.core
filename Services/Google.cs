﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

using OAuth2.Core.Models;
using OAuth2.Core.Helpers;

namespace OAuth2.Core.Services
{
    public class Google : ServiceBase
    {
        public override string SERVICE_NAME { get { return "Google"; } }

        public override string AUTHORIZATION_ENDPOINT { get { return "https://accounts.google.com/o/oauth2/v2/auth"; } }
        public override string TOKEN_ENDPOINT { get { return "https://www.googleapis.com/oauth2/v4/token"; } }
        public override string USERINFO_ENDPOINT { get { return "https://www.googleapis.com/oauth2/v3/userinfo"; } }

        
        protected override IDictionary<string, string> GetAuthRequestParams(ICollection<string> scopes)
        {
            List<string> requested_scopes = new List<string>() {"profile", "email"};

            if (null != scopes)
                requested_scopes.AddRange(scopes);

            return new Dictionary<string, string>()
            {
                { "prompt", "consent"},
                { "access_type", "offline"},
                { "scope", String.Join(" ", requested_scopes) }
            };
        }

        protected override TokenInfo ParseTokenResponse(string response)
        {
            Dictionary<string, string> tk = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            if(!tk.ContainsKey("refresh_token"))
            {
                throw new ReRunAuthenticationException(new Dictionary<string, string>() { { "prompt", null }, { "approval_prompt", "force" },  { "access_type", "offline" } });
            }

            return new TokenInfo()
            {
                AccessToken = tk["access_token"],
                RefreshToken = tk["refresh_token"],
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(TimeSpan.FromSeconds(int.Parse(tk["expires_in"])))
            };
        }

        protected override TokenInfo ParseRefreshToken(string response)
        {
            Dictionary<string, string> tk = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            return new TokenInfo()
            {
                AccessToken = tk["access_token"],
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(TimeSpan.FromSeconds(int.Parse(tk["expires_in"])))
            };
        }

        protected override UserInfo ParseUserInfoResponse(string response)
        {
            Dictionary<string, string> ui = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            return new UserInfo()
            {
                Provider = "Google",
                ProviderId = ui.Get("sub"),
                Name = ui.Get("name"),
                FirstName = ui.Get("given_name"),
                LastName = ui.Get("family_name"),
                Email = ui.Get("email"),
                Profile = ui.Get("profile"),
                Picture = ui.Get("picture"),
                Gender = ui.Get("gender"),
                Locale = ui.Get("locale")
            };
        }
    }
}
