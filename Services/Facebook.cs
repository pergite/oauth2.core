﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

using OAuth2.Core.Models;

namespace OAuth2.Core.Services
{
    public class Facebook : ServiceBase
    {
        public override string SERVICE_NAME { get { return "Facebook"; } }

        public override string AUTHORIZATION_ENDPOINT { get { return "https://www.facebook.com/dialog/oauth"; } }
        public override string TOKEN_ENDPOINT { get { return "https://graph.facebook.com/oauth/access_token"; } }
        public override string USERINFO_ENDPOINT { get { return "https://graph.facebook.com/me"; } }

        protected override IDictionary<string, string> GetAuthRequestParams(ICollection<string> scopes)
        {
            List<string> requested_scopes = new List<string>() { "email" };

            if (null != scopes)
                requested_scopes.AddRange(scopes);


            return new Dictionary<string, string>()
            {
                { "scope", String.Join(" ", requested_scopes) },
                { "auth_type", "reauthenticate" }
            };
        }

        protected override TokenInfo ParseTokenResponse(string response)
        {
            Dictionary<string, string> tk = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            return new TokenInfo()
            {
                AccessToken = tk["access_token"],
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(TimeSpan.FromSeconds(int.Parse(tk["expires_in"])))
            };
        }

        protected override UserInfo ParseUserInfoResponse(string response)
        {
            Dictionary<string, string> ui = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            return new UserInfo()
            {
                Provider = "Facebook",
                ProviderId = ui["id"],
                Name = ui["name"],
                FirstName = ui["name"]
            };
        }
    }
}