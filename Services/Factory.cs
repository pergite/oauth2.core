﻿using System;
using System.Text;
using System.Security.Cryptography;

using OAuth2.Core.Models;
using OAuth2.Core.Helpers;

namespace OAuth2.Core.Services
{
    public static class Factory
    {
        public static ServiceBase Get(ISettings settings)
        {
            return Get(settings, null);
        }

        public static ServiceBase Get(ISettings settings, ITokenManager tm)
        {
            if (null != settings)
            {

                // a somewhat naive implementation...
                switch (settings.ServiceName.ToLower())
                {
                    case "google":

                        return new Google()
                        {
                            CLIENT_ID = settings.ClientID,
                            CLIENT_SECRET = settings.ClientSecret,
                            TokenManager = tm
                        };

                    case "facebook":

                        return new Facebook()
                        {
                            CLIENT_ID = settings.ClientID,
                            CLIENT_SECRET = settings.ClientSecret,
                            TokenManager = tm
                        };

                    case "microsoft":

                        return new Microsoft()
                        {
                            CLIENT_ID = settings.ClientID,
                            CLIENT_SECRET = settings.ClientSecret,
                            TokenManager = tm
                        };
                }
            }
            return null;
        }
    }
}
