﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using Starcounter;

using OAuth2.Core.Models;
using OAuth2.Core.Services;
using OAuth2.Core.Helpers;

namespace OAuth2.Core.Handlers
{
    public class OAuth2Flow
    {
        public static void Register(Uri appRoot, ISettingsManager settingsManager, IUserManager userManager)
        {
            Func<string, string, Request, Response> redirectToAuth = (string serviceName, string queryString, Request request) =>
            {
                ObSession session = SessionManager.NewSession();

                ServiceBase p = Factory.Get(settingsManager.GetSettings(serviceName));

                if (p == null)
                    return Response.FromStatusCode(404);

                List<string> scope = new List<string>();

                if (!String.IsNullOrEmpty(queryString))
                {
                    NameValueCollection query = HttpUtility.ParseQueryString(queryString);

                    if (null != query["scope"])
                    {
                        scope.Add(query["scope"]);

                        session["scope"] = scope;
                    }

                    if (null != query["final_destination"])
                    {
                        session["final_destination"] = HttpUtility.UrlDecode(query["final_destination"]);
                    }
                }

                var response = ResponseHelper.Redirect(p.GetAuthEndpoint(session, request.MakeAbsoluteUrl(appRoot.Combine($"{serviceName}/callback")), "", scope));

                return SessionManager.SetOnResponse(response, session);
            };

            Handle.GET(appRoot.Combine("{?}/signin?{?}"), redirectToAuth);
            Handle.GET(appRoot.Combine("{?}/signin"), (string serviceName, Request request) =>
            {
                return redirectToAuth(serviceName, null, request);
            });

            Handle.GET(appRoot.Combine("{?}/callback?{?}"), (string serviceName, string queryString, Request request) => {

                ObSession session = SessionManager.GetFromRequest(request);

                if (null == session)
                {
                    // No session, probably too long since signin called "redirect" back to signinpage
                    // Re-run the oauth signin step
                    return Self.GET(appRoot.Combine($"/{serviceName}/signin"));
                }

                var scope = session["scope"] as ICollection<string>;

                ServiceBase p = Factory.Get(settingsManager.GetSettings(serviceName));

                AuthResponseResult result = p.ProcessAuthResponse(session, queryString, request.MakeAbsoluteUrl(appRoot.Combine($"{serviceName}/callback")), "", scope);

                if (!result)
                {
                    if (result.ShouldReRunAuthentication && !session.ContainsKey("rerun"))
                    {
                        session["rerun"] = true;
                        var response = ResponseHelper.Redirect(p.GetAuthEndpoint(session, request.MakeAbsoluteUrl(appRoot.Combine($"{serviceName}/callback")), "", scope, result.ReRunAuthentication.AdditionalRequestParameters));

                        return SessionManager.SetOnResponse(response, session);
                    }
                }

                result.FinalDestination = session["final_destination"] as string;

                return userManager.ProcessAuthResponse(result);
            });
        }
    }
}
