﻿using System;
using Starcounter;

namespace OAuth2.Core.Models
{
    public interface ISettings
    {
        string ServiceName { get; set; }
        string ClientID { get; }
        string ClientSecret { get; }
    }

    public interface ISettingsManager
    {
        ISettings GetSettings(string serviceName);
    }

    public interface IUser
    {
        string Provider { get; set; }
        string ProviderId { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Profile { get; set; }
        string Picture { get; set; }
        string Gender { get; set; }
        string Locale { get; set; }
    }

    public interface IToken
    {
        string AccessToken { get; set; }
        string RefreshToken { get; set; }
        DateTime Issued { get; set; }
        DateTime Expires { get; set; }
    }
    
    public interface ITokenManager
    {
        IToken GetToken();
        void SetToken(IToken token);
    }

    public interface IUserManager
    {
        Response ProcessAuthResponse(AuthResponseResult authResult);
    }
}
