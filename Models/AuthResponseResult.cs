﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAuth2.Core.Models
{
    internal class ReRunAuthenticationException : Exception
    {
        public IDictionary<string, string> AdditionalRequestParameters;

        public ReRunAuthenticationException(IDictionary<string, string> additionalRequestParameters)
        {
            this.AdditionalRequestParameters = additionalRequestParameters;
        }
    }

    public class ReRunAuthentication
    {
        public IDictionary<string, string> AdditionalRequestParameters;

        public ReRunAuthentication(IDictionary<string, string> additionalRequestParameters)
        {
            this.AdditionalRequestParameters = additionalRequestParameters;
        }

    }

    public class AuthResponseResult
    {
        protected bool status;
        protected string message;
        protected ReRunAuthentication rerun;

        public string Message { get { return message; } }

        public IToken Token { get; internal set; }
        public IUser User { get; internal set; }

        public string FinalDestination { get; internal set; }

        public ReRunAuthentication ReRunAuthentication { get { return rerun; } }

        public AuthResponseResult(bool status)
        {
            this.status = status;
        }

        public static AuthResponseResult Error(string message = null, ReRunAuthentication rerun = null)
        {
            AuthResponseResult result = new AuthResponseResult(false);
            result.message = message;
            result.rerun = rerun;

            return result;
        }

        public static implicit operator bool(AuthResponseResult r)
        {
            return r.status;
        }

        public bool ShouldReRunAuthentication
        {
            get
            {
                return null != rerun;
            }
        }
    }
}
