﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OAuth2.Core.Models
{
    public class TokenInfo : IToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime Issued { get; set; }
        public DateTime Expires { get; set; }
    }
}
